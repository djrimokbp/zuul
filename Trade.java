/**
 * This class is part of the "World of Zuul" application. "World of Zuul" is a
 * very simple, text based adventure game.
 * 
 * This class represents the Trade command of the game. This command searches
 * for an NPC character in the room and replace its item with the Player's item
 * defined in the command.
 * 
 * @author Georgios kalogiros - gk1411
 * 
 */
public class Trade implements Action {

	/**
	 * This is the constructor of the Trade Class
	 */
	public Trade() {
	}

	/**
	 * The doAction function trades an item between the current player and the
	 * existing NPC in the room. if the specified item is not owned by the
	 * player, an error message is printed
	 * 
	 * @param player
	 *            --> the current player
	 */
	public void doAction(Player player, Command command) {

		Item e;
		NPC dwarf = player.getCurrentRoom().getNPC();

		if (!command.hasSecondWord()) {
			// if there is no second word, we don't know what to take...
			System.out.println("Trade what?");
			return;
		}

		String itemName = command.getSecondWord();

		if (player.doItemExist(itemName)) {
			e = player.getItem(itemName);
			player.removeItem(e);
			player.addItem(dwarf.tradeItem(e));
		} else {
			System.out.println("Player does not have this item!");
		}
		player.getCurrentRoom().printInfo();
		player.getCurrentRoom().printItemsInfo();
		player.printInfo();
		player.getCurrentRoom().getNPC().printInfo();
	}
}
