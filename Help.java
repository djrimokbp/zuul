/**
 * Print out some help information. Here we print some stupid, cryptic message
 * and a list of the command words.
 */
public class Help implements Action {
	CommandWords wrds = new CommandWords();
	
	/**
	 * Constructor of class Help
	 */
	public Help() {
	}



	/**
	 * This method prints help messages
	 * 
	 * @param player
	 * @param command
	 */
	public void doAction(Player player, Command command) {
		System.out.println("You are lost. You are alone. You wander");
		System.out.println("around at the university.");
		System.out.println();
		System.out.println("Your command words are:");
	}
}
