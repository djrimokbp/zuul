/**
 * Class GameObject - Represents an Object in the game.
 * 
 * This class is part of the "World of Zuul" application. "World of Zuul" is a
 * very simple, text based adventure game.
 * 
 * A GameObject provides an object of this Game with a main functionality (name,
 * description) As object is considered a player, an item, an NPC or a Room.
 * 
 * This Class contributes to the extensibility of this game
 * 
 * @author Georgios Kalogiros - gk1411
 * @version 1.0 (April 2012)
 */

public abstract class GameObject {
	private String name;
	private String description;

	/**
	 * This is the constructor of the class GameObject Provide Each Object of
	 * the game with a name and a description.
	 * 
	 * @param name
	 * @param description
	 */
	public GameObject(String name, String description) {
		this.name = name;
		this.description = description;

	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
