/**
 * 
 * This class is part of the "World of Zuul" application. "World of Zuul" is a
 * very simple, text based adventure game.
 * 
 * An "Item" represents a physical object that exist in a Room .
 * 
 * The class Item has two distinct elements, name and description.
 * 
 * @author Georgios Kalogiros - gk1411
 */
public class Item extends GameObject {

	/**
	 * This is the constructor of the class Item Create an Item with the name
	 * "name" and described as "description".
	 * 
	 * @param name
	 *            , description
	 */

	public Item(String name, String description) {
		super(name, description);
	}

	public String getName() {
		return super.getName();
	}

	public String getDescription() {
		return super.getDescription();
	}
}
