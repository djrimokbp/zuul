import java.util.ArrayList;
import java.util.Set;
import java.util.HashMap;

/**
 * Class Room - a room in an adventure game.
 * 
 * This class is part of the "World of Zuul" application. "World of Zuul" is a
 * very simple, text based adventure game.
 * 
 * A "Room" represents one location in the scenery of the game. It is connected
 * to other rooms via exits. For each existing exit, the room stores a reference
 * to the neighboring room.
 * 
 * The class Room extends the Class GameObject
 * 
 * @author Michael Kolling and David J. Barnes - Edited by Georgios Kalogiros
 * @version 1.0 (February 2002)
 */

public class Room extends GameObject {

	private HashMap<String, Exit> exits; // stores exits of this room.
	private ArrayList<Item> itemsList; // stores items of this room.
	private NPC dwarf; // this is the NPC of this room.

	/**
	 * Create a room described and add a description. 
     * "description" is something like "in a kitchen" or "in an open court
	 * yard".
     * The room initially, it has no exits.
	 * 
	 * @param desciption
	 */
	public Room(String name, String description) {
		super(name, description);
		// Exits is a HashMap where all the doors/Exits of this Room are stored.
		exits = new HashMap<String, Exit>();
		// itemsList is an array in which they are stored all the items
		// positioned in this room.
		itemsList = new ArrayList<Item>(0);
	}

	/**
	 * NPC getter
	 * 
	 * @return NPC
	 */
	public NPC getNPC() {
		return dwarf;
	}

	/**
	 * NPC setter sets the NPC object of this room
	 * 
	 * @param n
	 */
	public void setNPC(NPC n) {
		this.dwarf = n;
	}

	/**
	 * Define an exit from this room.
	 */
	public void setExit(String direction, Exit neighborExits) {
		exits.put(direction, neighborExits);
	}

	/**
	 * Return the description of the room (the one that was defined in the
	 * constructor).
	 */
	public String getShortDescription() {
		return super.getDescription();
	}

	/**
	 * Return a long description of this room, in the form: You are in the
	 * kitchen. Exits: north west
	 */
	public String getLongDescription() {
		return "You are " + super.getDescription() + ".\n" + getExitString();
	}

	/**
	 * Return a string describing the room's exits, for example
	 * "Exits: north west".
	 */
	private String getExitString() {
		String returnString = "Exits:";
		Set<String> keys = exits.keySet();
		for (String exit : keys)
			returnString += " " + exit;
		return returnString;
	}

	/**
	 * This function displays on the screen the exits that this room has.
	 */
	public void printInfo() {
		String str = "Exits:";
		Set<String> keys = exits.keySet();
		for (String exit : keys)
			str += " " + exit;
		System.out.println(str);
	}

	/**
	 * This function displays on the screen the items that exist in this room.
	 */
	public void printItemsInfo() {
		String str = "In this Room there are the following items:";

		for (Item e : itemsList)
			str += " " + e.getName();
		System.out.println(str);
	}

	/**
	 * Return the room that is reached if we go from this room in direction
	 * "direction". If there is no room in that direction, return null.
	 */
	public Exit getExit(String direction) {
		return exits.get(direction);
	}

	/**
	 * Each Room has a collection of Items. Use this function in order to check
	 * if this item exists in the Collection.
	 * 
	 * @param itemName
	 * @return doExist
	 * 
	 */
	public boolean doItemExist(String itemName) {

		boolean doExist = false;
		String name;

		for (Item e : itemsList) {
			name = e.getName();
			if (name.compareTo(itemName) == 0) {
				doExist = true;
			}
		}

		return doExist;
	}

	/**
	 * Each Room has a collection of Items. Use this function in order to get
	 * the object with a specific name.
	 * 
	 * It takes as input the name of an item and returns the object in which the
	 * name and the description of the item are stored
	 * 
	 * @param itemName
	 * @return returnedItem
	 * 
	 */
	public Item getItem(String itemName) {
		String name;
		Item returnedItem = null;

		for (Item e : itemsList) {
			name = e.getName();
			if (name.compareTo(itemName) == 0) {
				returnedItem = e;
			}
		}
		return returnedItem;
	}

	/**
	 * Each Room has a collection of Items. Use this function in order to add
	 * items in this collection.
	 * 
	 * @param e
	 */
	public void addItem(Item e) {
		itemsList.add(e);
	}

	/**
	 * Each Room has a collection of Items. Use this function to remove the
	 * specific item from this collection. If two or more Items found in this
	 * collection, only one of them will be removed.
	 * 
	 * @param e
	 */
	public void removeItem(Item e) {

		// Search the List of the Items
		for (int i = 0; i < itemsList.size(); i++) {
			// If the item e is found..
			if (itemsList.get(i).equals(e)) {
				/*
				 * ...Remove the Item and break the searching procedure so that
				 * in case where two or more same items exist in the room, only
				 * the first in the array list is removed
				 */
				itemsList.remove(i);
				break;
			}
		}

	}

}
