/**
 * This class is part of the "World of Zuul" application. "World of Zuul" is a
 * very simple, text based adventure game.
 * 
 * This class represents the Unlock command of the game. It is used in order for
 * the player to unlock an exit which exists in a room.
 * 
 * @author Georgios kalogiros - gk1411
 * 
 */
public class Unlock implements Action {

	/**
	 * This is the constructor of the Unlock class
	 */
	public Unlock() {
	}

	/**
	 * Try to unlock a door/exit in the room if the specified room is not found,
	 * type an error message
	 * 
	 * @param player
	 */
	public void doAction(Player player, Command command) {
		boolean existKey = false;
		Exit ex = null;
		if (!command.hasSecondWord()) {
			// if there is no second word, we don't know what to lock...
			System.out.println("Unlock what?");
			return;
		}

		// Get the second word of the Command
		String exitToUnlock = command.getSecondWord();
		ex = player.getCurrentRoom().getExit(exitToUnlock);
		// Check if the player has a key Item
		existKey = player.doItemExist("key");
		try {
			// if there is such Exit and the Player has a key Item
			if (existKey == true) {
				if (ex.isLocked() == true) {
					// Unlock the door
					player.getCurrentRoom().getExit(exitToUnlock)
							.setLocked(false);
				} else {
					System.out.println("The Exit is already unlocked!");
				}
			} else {
				System.out.println("The player do not have a key Item!");
			}
		} catch (NullPointerException e) {
			System.out.println("This Exit does not exist! Try another exit!");
		}
		player.getCurrentRoom().printInfo();
		player.getCurrentRoom().printItemsInfo();
		player.printInfo();
		player.getCurrentRoom().getNPC().printInfo();
	}

}
