/**
 * Class Exit - an exit of the room in an adventure game.
 * 
 * This class is part of the "World of Zuul" application. "World of Zuul" is a
 * very simple, text based adventure game.
 * 
 * An "Exit" represents a door/exit in the scenery of the game. This exit is
 * located in a "Room". Room could have a lot of exits.
 * 
 * @author Georgios Kalogiros - gk1411
 * @version 1.0 (May 2012)
 */
public class Exit extends GameObject {
	private Room currentRoom;
	private Room room;
	private boolean isLocked;
	private boolean isLockable;

	/**
	 * This is the constructor of the Exit class. Use this constructor to create
	 * an exit which is lockable.
	 * 
	 * @param name
	 * @param current
	 * @param next
	 * @param isLocked
	 * @param isLockable
	 */
	public Exit(String name, String description, Room current, Room next,
			boolean isLocked) {
		super(name, description);
		this.setCurrentRoom(current);
		this.setNextRoom(next);
		this.setLocked(isLocked);
		this.isLockable = true;
	}

	/**
	 * This is the constructor of the Exit class. Use this constructor to create
	 * an exit which is not lockable.
	 * 
	 * @param name
	 * @param current
	 * @param next
	 * @param isLocked
	 */
	public Exit(String name, String description, Room current, Room next) {
		super(name, description);
		this.setCurrentRoom(current);
		this.setNextRoom(next);
		this.setLocked(false);
		this.isLockable = false;
	}

	/**
	 * This function sets the name of current Exit.
	 * 
	 * @param name
	 */
	public void setName(String name) {
		super.setName(name);
	}

	/**
	 * Get the name of the current Exit. Returns a string denoting the direction
	 * of this door.
	 * 
	 * @return nameOfExit
	 */
	public String getName() {
		return super.getName();
	}

	/**
	 * This function sets the room in which this exit leads.
	 * 
	 * @param room
	 */
	public void setNextRoom(Room room) {
		this.room = room;
	}

	/**
	 * This function gets the room in which this exit leads.
	 * 
	 * @return room
	 */
	public Room getNextRoom() {
		return room;
	}

	/**
	 * This function sets the door Locked or Unlocked isLocked should be true if
	 * we want the door locked or false otherwise.
	 * 
	 * @param isLocked
	 */
	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	/**
	 * This function indicates if this Exit is locked or not. Returns false if
	 * the Exit is Unlocked or true if it is locked.
	 * 
	 * @return isLocked
	 */
	public boolean isLocked() {
		return isLocked;
	}

	/**
	 * This function indicates if this Exit is lockable or not Returns false if
	 * the Exit is unlockable or true if it is lockable
	 * 
	 * @return true or false
	 */
	public boolean isLockable() {
		return this.isLockable;
	}

	/**
	 * Getter for the private variable currentRoom
	 * 
	 * @return currentRoom object
	 */
	public Room getCurrentRoom() {
		return currentRoom;
	}

	/**
	 * Setter for the private variable currentRoom
	 * 
	 * @params currentRoom
	 */
	public void setCurrentRoom(Room currentRoom) {
		this.currentRoom = currentRoom;
	}
}
