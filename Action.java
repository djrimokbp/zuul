/**
 * This interface is part of the "World of Zuul" application. "World of Zuul" is
 * a very simple, text based adventure game.
 * 
 * This interface illustrates the functions that should be implemented by each
 * command class e.g. Go, Take, Lock etc.
 * 
 * @author Georgios Kalogiros - gk1411
 * @version 1.0 (May 2012)
 */
public interface Action {

	/**
	 * This method should be implemented by a class that denotes a command
	 */
	public void doAction(Player player, Command command);
}
