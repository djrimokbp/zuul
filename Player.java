import java.util.ArrayList;

/**
 * 
 * This class is part of the "World of Zuul" application. "World of Zuul" is a
 * very simple, text based adventure game.
 * 
 * A "Player" represents a physical object that exists in a Room .
 * 
 * The class Player extends the Class GameObject
 * 
 * @author Georgios Kalogiros - gk1411
 */
public class Player extends GameObject {
	private Room currentRoom;
	private ArrayList<Item> itemsList;

	/**
	 * Create a player with the name "name" and described "description".
	 * Initially, the player has no items but they can added later by using the
	 * function addItem(Item e).
	 * 
	 * @param name
	 * @param description
	 */
	public Player(String name, String description) {
		super(name, description);

		/*
		 * Initialize the ArrayList which holds the items Its initial size is 0
		 */
		itemsList = new ArrayList<Item>(0);
	}

	/**
	 * Set the current Room where the player is
	 * 
	 * @param room
	 */
	public void setCurrentRoom(Room room) {
		currentRoom = room;
	}

	/**
	 * Get the current Room where the player is
	 * 
	 * @return current Room
	 */
	public Room getCurrentRoom() {
		return this.currentRoom;
	}

	/**
	 * Each Player has a collection of Items. Use this function in order to
	 * check if this item exists in the Collection.
	 * 
	 * Returns true if the specified Item exists in the Collection.
	 * 
	 * @param itemName
	 * @return doExist
	 * 
	 */
	public boolean doItemExist(String itemName) {
		boolean doExist = false;
		String name;

		for (Item e : itemsList) {
			name = e.getName();
			/*
			 * If the item is found in the ArrayList make the variable doExist
			 * equals true
			 */
			if (name.compareTo(itemName) == 0) {
				doExist = true;
			}
		}
		return doExist;
	}

	/**
	 * Each Player has a collection of Items. Use this function in order to get
	 * the object with a specific name.
	 * 
	 * @param itemName
	 * @return returnedItem
	 * 
	 */
	public Item getItem(String itemName) {
		String name;
		Item returnedItem = null;

		for (Item e : itemsList) {
			name = e.getName();
			// If the Item found...
			if (name.compareTo(itemName) == 0) {
				returnedItem = e;
				// ...Stop searching the Arraylist
				break;
			}
		}
		return returnedItem;
	}

	/**
	 * Each Player has a collection of Items. Use this function in order to add
	 * items in this collection.
	 * 
	 * @param e
	 */
	public void addItem(Item e) {
		itemsList.add(e);
	}

	/**
	 * Each Player has a collection of Items. Use this function to remove the
	 * specific item from this collection. If two or more Items found in this
	 * collection, only one of them will be removed.
	 * 
	 * @param e
	 */
	public void removeItem(Item e) {
		/*
		 * Removal of the first Item with the name e that this Arraylist holds
		 */
		for (int i = 0; i < itemsList.size(); i++) {
			if (itemsList.get(i).equals(e)) {
				itemsList.remove(i);
				break;
			}
		}
	}

	/**
	 * This function displays on the screen the items that this player has.
	 */
	public void printInfo() {
		String str = "";
		for (Item e : itemsList) {
			str += " " + e.getName();
		}
		if (str == "") {
			System.out.println("You don't have any items");
		} else {
			System.out.println("You have the following items:" + str);
		}
	}

}
