/**
 * This class is part of the "World of Zuul" application. "World of Zuul" is a
 * very simple, text based adventure game.
 * 
 * This class represents the Take command of the game It is used in order for
 * the player to take items from the current room.
 * 
 * This class should implement the Action interface.
 * 
 * @author Georgios kalogiros - gk1411
 * 
 */
public class Take implements Action {

	/**
	 * This is the constructor of the Take class.
	 */
	public Take() {
	}

	/**
	 * Try to take an item from the room if the specified item is not found type
	 * an error message
	 * 
	 * @param player
	 */
	public void doAction(Player player, Command command) {

		Item e;

		if (!command.hasSecondWord()) {
			// if there is no second word, we don't know what to take...
			System.out.println("Take what?");
			return;
		}

		String itemName = command.getSecondWord();

		if (player.getCurrentRoom().doItemExist(itemName)) {
			e = player.getCurrentRoom().getItem(itemName);
			// Remove Item from this Room.
			player.getCurrentRoom().removeItem(e);
			// Add Item in Player's Items List
			player.addItem(e);
		} else {
			System.out.println("Do not exist this item in this room!");
			return;
		}
		player.getCurrentRoom().printInfo();
		player.getCurrentRoom().printItemsInfo();
		player.printInfo();
		player.getCurrentRoom().getNPC().printInfo();
	}
}
