/**
 * This class is the main class of the "World of Zuul" application.
 * "World of Zuul" is a very simple, text based adventure game. Users can walk
 * around some scenery. That's all. It should really be extended to make it more
 * interesting!
 * 
 * To play this game, create an instance of this class and call the "play"
 * method.
 * 
 * This main class creates and initialises all the others: it creates all rooms,
 * creates the parser and starts the game. It also evaluates and executes the
 * commands that the parser returns.
 * 
 * @author Michael Kolling and David J. Barnes (Edited by Georgios Kalogiros)
 * @version 1.0 (February 2002)
 */

public class Game {
	private Parser parser;
	private Player player;

	/**
	 * Create the game and initialise its internal map.
	 */
	public Game() {
		initialiseGame();
	}

	/**
	 * This function initialises the game Creates a Player Creates the Rooms of
	 * the Game Creates the Exits for each room Creates the NPCs Creates Items
	 */
	private void initialiseGame() {
		parser = new Parser();
		player = new Player("George", "This is a test Player");

		Room outside, theatre, pub, lab, office;
		Item key = new Item("key", "This is a key owned by the dwarf");
		Item gold = new Item("gold", "This is a gold item");

		// create the rooms
		outside = new Room("outside",
				"outside the main entrance of the university");
		theatre = new Room("theatre", "in a lecture theatre");
		pub = new Room("pub", "in the campus pub");
		lab = new Room("lab", "in a computing lab");
		office = new Room("office", "in the computing admin office");

		// initialise outside room exits
		outside.setExit("east", new Exit("east", "This is the east exit",
				outside, theatre, false));
		outside.setExit("south", new Exit("south", "This is the south exit",
				outside, lab, false));
		outside.setExit("west", new Exit("west", "This is the west exit",
				outside, pub, false));

		// Add items to room outside
		outside.addItem(key);
		outside.addItem(gold);

		// Create NPC for the room outside
		NPC outsideNPC = new NPC("John", "I am the dwarf of the outside room",
				key);
		outside.setNPC(outsideNPC);

		// initialise theatre room exits
		theatre.setExit("west", new Exit("west", "This is the west exit",
				theatre, outside, false));

		// Add items to room theatre
		theatre.addItem(key);
		theatre.addItem(gold);

		// Create NPC for the room theatre
		NPC theatreNPC = new NPC("Mike", "I am the dwarf of the theatre room",
				key);
		theatre.setNPC(theatreNPC);

		// initialise pub room exits
		pub.setExit("east", new Exit("east", "This is the east exit", pub,
				outside, false));
		pub.addItem(gold);

		// Add items to room pub
		pub.addItem(key);
		pub.addItem(gold);

		// Create NPC for the room pub
		NPC pubNPC = new NPC("John", "I am the dwarf of the pub room", key);
		pub.setNPC(pubNPC);

		// initialise lab room exits
		lab.setExit("north", new Exit("north", "This is the north exit", lab,
				outside, false));
		lab.setExit("east", new Exit("east", "This is the east exit", lab,
				office, false));

		// Add items to room lab
		lab.addItem(key);
		lab.addItem(gold);

		// Create NPC for the room lab
		NPC labNPC = new NPC("John", "I am the dwarf of the pub room", key);
		lab.setNPC(labNPC);

		// initialise office room exits
		office.setExit("west", new Exit("west", "This is the west exit",
				office, lab, false));

		// Create NPC for the room office
		NPC officeNPC = new NPC("John", "I am the dwarf of the pub room", key);
		office.setNPC(officeNPC);

		player.setCurrentRoom(outside); // start game outside
		player.addItem(new Item("map", "This is a map item"));
		player.addItem(new Item("gold", "This is a gold item"));
	}

	/**
	 * Main play routine. Loops until end of play.
	 */
	public void play() {
		printWelcome();

		// Enter the main command loop. Here we repeatedly read commands and
		// execute them until the game is over.

		boolean finished = Command.getWantToQuit();
		while (!finished) {
			Command command = parser.getCommand();
			finished = processCommand(command);
		}
		System.out.println("Thank you for playing.  Good bye.");
	}

	/**
	 * Print out the opening message for the player.
	 */
	private void printWelcome() {
		System.out.println();
		System.out.println("Welcome to the World of Zuul!");
		System.out
				.println("World of Zuul is a new, incredibly boring adventure game.");
		System.out.println("Type 'help' if you need help.");
		System.out.println();
		System.out.println(player.getCurrentRoom().getLongDescription());
	}

	/**
	 * Given a command, process (that is: execute) the command. If this command
	 * ends the game, true is returned, otherwise false is returned.
	 */
	private boolean processCommand(Command command) {

		if (command.isUnknown()) {
			System.out.println("I don't know what you mean...");
			return false;
		}

		String commandWord = command.getCommandWord();

		CommandWords cw = new CommandWords();

		// Create valid commands
		cw.constructCommands();
		// If command exists...
		if (cw.isCommand(commandWord)) {
			// ...execute command
			cw.getCommand(commandWord).doAction(player, command);
		} else {
			System.out.println("I don't know what you mean...");
		}

		return Command.getWantToQuit();
	}

	public static void main(String[] args) {
		Game game = new Game();
		game.play();
	}

}
