/**
 * This class is part of the "World of Zuul" application. "World of Zuul" is a
 * very simple, text based adventure game.
 * 
 * This class represents the Drop command of the game It is used in order for
 * the player to drop items in the current room.
 * 
 * @author Georgios kalogiros - gk1411
 * 
 */

public class Drop implements Action {

	/**
	 * Constructor of class Drop
	 */
	public Drop() {
	}

	/**
	 * Try to drop an item in the room if the specified item is not found on the
	 * player type an error message
	 * 
	 * @param player
	 * @param command
	 */
	public void doAction(Player player, Command command) {

		Item e;

		// this command requires a second word
		if (!command.hasSecondWord()) {
			// if there is no second word, we don't know what to take...
			System.out.println("Drop what?");
			return;
		}

		// This is the item that the player wants to drop
		String itemName = command.getSecondWord();

		// If this item exists in player's itemlist
		if (player.doItemExist(itemName)) {
			e = player.getItem(itemName);
			// remove the item
			player.removeItem(e);
			// ...and add it in the current Room's ItemList
			player.getCurrentRoom().addItem(e);
		} else {
			System.out.println("Player does not have this item!");
			return;
		}

		player.getCurrentRoom().printInfo();
		player.getCurrentRoom().printItemsInfo();
		player.printInfo();
		player.getCurrentRoom().getNPC().printInfo();
	}
}
