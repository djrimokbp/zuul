import java.util.HashMap;

/**
 * This class is part of the "World of Zuul" application. "World of Zuul" is a
 * very simple, text based adventure game.
 * 
 * This class holds an enumeration of all the actions/commands that a player can
 * do in the game It is used to recognise commands as they are typed in.
 * 
 * @author Michael Kolling and David J. Barnes (Edited by Georgios Kalogiros)
 * @version 1.0 (February 2002)
 */

public class CommandWords {
	private HashMap<String, Action> cmdList;

	/**
	 * Constructor - initialise the command words.
	 */
	public CommandWords() {

	}

	/**
	 * This is a method that constructs one object for each word in the game
	 * Afterwards they are inserted in a HashMap stored in this class. Each
	 * valid command should be inserted in the HashMap
	 * 
	 */
	public void constructCommands() {

		cmdList = new HashMap<String, Action>();

		Action execHelp = new Help();
		cmdList.put("help", execHelp);

		Action execQuit = new Quit();
		cmdList.put("quit", execQuit);

		Action execGo = new Go();
		cmdList.put("go", execGo);

		Action execTake = new Take();
		cmdList.put("take", execTake);

		Action execDrop = new Drop();
		cmdList.put("drop", execDrop);

		Action execTrade = new Trade();
		cmdList.put("trade", execTrade);

		Action execLock = new Lock();
		cmdList.put("lock", execLock);

		Action execUnlock = new Unlock();
		cmdList.put("unlock", execUnlock);
	}

	/**
	 * Check whether a given String is a valid command word. Return true if it
	 * is, false if it isn't.
	 */
	public boolean isCommand(String aString) {

		if (cmdList.containsKey(aString)) {
			return true;
		}

		// if we get here, the string was not found in the commands
		return false;
	}

	/**
	 * Returns the Action object associated to the key command Actions are the
	 * commands that a player can do in the game
	 * 
	 * @param command
	 * @return
	 */
	public Action getCommand(String command) {
		return cmdList.get(command);
	}

	/**
	 * Print all valid commands to System.out.
	 */
	public void showAll() {
		System.out.println("go, help, quit, trade, take, drop, lock, unlock. ");

	}

}
