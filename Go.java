/**
 * @author Georgios Kalogiros - gk1411
 */
public class Go implements Action {

	/**
	 * Constructor of class Go
	 */
	public Go() {
	}

	/**
	 * Try to go to one direction. If there is an exit, enter the new room,
	 * otherwise print an error message.
	 * 
	 * @param player
	 * @param command
	 */
	public void doAction(Player player, Command command) {
		if (!command.hasSecondWord()) {
			// if there is no second word, we don't know where to go...
			System.out.println("Go where?");
			return;
		}

		String direction = command.getSecondWord();

		// Try to leave current room.
		// Room nextRoom = player.currentRoom.getExit(direction);

		try {
			// Returns the exit object that player requested
			Exit exit = player.getCurrentRoom().getExit(direction);
			// Returns the room to Where the selected exit leads.
			Room nextRoom = exit.getNextRoom();
			// if there is not such room print an error message
			if (nextRoom == null)
				System.out.println("There is no door!");
			// Check if this Exit is locked.
			else if (exit.isLocked()) {
				System.out
						.println("This door is Locked. Try to unlock it first!");
			} else {
				player.setCurrentRoom(nextRoom);
				System.out
						.println(player.getCurrentRoom().getLongDescription());
			}
		} catch (NullPointerException e) {
			System.out.println("There is no such exit to " + direction + ".");
		}
		player.getCurrentRoom().printItemsInfo();
		player.printInfo();
		player.getCurrentRoom().getNPC().printInfo();
	}
}
