/**
 * This class is part of the "World of Zuul" application. "World of Zuul" is a
 * very simple, text based adventure game.
 * 
 * This class represents the Lock command of the game. It is used in order for
 * the player to lock an exit which exists in a room.
 * 
 * * This class should implement the Action interface.
 * 
 * @author Georgios kalogiros - gk1411
 * 
 */
public class Lock implements Action {

	/**
	 * This is the constructor of the Class
	 */
	public Lock() {
	}

	/**
	 * Try to lock a door in the room if the specified room is not found, type
	 * an error message
	 * 
	 * @param player
	 * @param command
	 */
	public void doAction(Player player, Command command) {
		Exit ex = null;
		boolean existKey = false;

		if (!command.hasSecondWord()) {
			// if there is no second word, we don't know what to lock...
			System.out.println("Lock what?");
			return;
		}

		String exitToLock = command.getSecondWord();

		ex = player.getCurrentRoom().getExit(exitToLock);

		existKey = player.doItemExist("key");
		// Check if the player has a key Item
		try {
			if (existKey == true && ex.isLocked() == false) {
				// Check if the Exit is lockable
				if (ex.isLockable()) {
					// lock the Exit
					player.getCurrentRoom().getExit(exitToLock).setLocked(true);

				} else {
					System.out
							.println("This exit cannot be locked! Try another direction");
				}
			} else {
				System.out
						.println("The player do not have a key Item or this exit is locked already!");
			}
		} catch (NullPointerException e) {
			System.out.println("This Exit does not exist! Try another Exit!");
		}
		player.getCurrentRoom().printInfo();
		player.getCurrentRoom().printItemsInfo();
		player.printInfo();
		player.getCurrentRoom().getNPC().printInfo();
	}
}
