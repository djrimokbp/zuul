/**
 * 
 * This class is part of the "World of Zuul" application. "World of Zuul" is a
 * very simple, text based adventure game.
 * 
 * An "Item" represents a physical object that exist in a Room .
 * 
 * The class Item has two distinct elements, name and description.
 * 
 * @author Georgios Kalogiros - gk1411
 */
public class NPC extends GameObject {

	private Item item;

	/**
	 * Constructor of the NPC class Creates an Non Player Character with the
	 * Name name and Description description
	 * 
	 * @param name
	 * @param description
	 * @param e
	 */
	public NPC(String name, String description, Item e) {
		super(name, description);
		// Assign an Item to the NPC
		setItemName(e);
	}

	/**
	 * This function takes as input the item that the Player wants to exchange.
	 * It Returns the item that the NPC character have
	 * 
	 * @param e
	 * @return exchangedItem
	 */
	public Item tradeItem(Item e) {
		Item exchangedItem = item;
		item = e;
		return exchangedItem;
	}

	/**
	 * Setter for the name of the NPC character
	 * 
	 * @param name
	 */
	public void setName(String name) {
		super.setName(name);
	}

	/**
	 * Getter for the name of the NPC character
	 * 
	 * @return name of this NPC.
	 */
	public String getName() {
		return super.getName();
	}

	/**
	 * Getter for the item that this NPC holds
	 * 
	 * @return --> Name of the item
	 */
	public String getItemName() {
		return this.item.getName();
	}

	/**
	 * Setter for the item that this NPC holds Return the name of the item
	 * 
	 * @return nameOfItem
	 */
	public void setItemName(Item e) {
		this.item = e;
	}

	/**
	 * Setter for the description of the NPC character
	 */
	public void setDescription(String description) {
		super.setDescription(description);
	}

	/**
	 * Getter for the description of the NPC character
	 */
	public String getDescription() {
		return super.getDescription();
	}

	/**
	 * This function displays on the screen the name of the NPC and what Item it
	 * stores.
	 */
	public void printInfo() {
		System.out.println("In this room there is a NPC whose name is "
				+ super.getName() + " and stores a/an " + this.getItemName());
	}
}
