/**
 * 
 * This class is part of the "World of Zuul" application. "World of Zuul" is a
 * very simple, text based adventure game.
 * 
 * This class represents the Quit command of the game.
 * 
 * This class should implement the Action interface.
 * 
 * @author Georgios kalogiros - gk1411
 * 
 */
public class Quit implements Action {

	/**
	 * This is the constructor of the Wuit class
	 */
	public Quit() {

	}

	/**
	 * This method implements the Quit Command
	 * 
	 * @param player
	 * @param command
	 */
	public void doAction(Player player, Command command) {
		if (!command.hasSecondWord()) {
			Command.setWantToQuit(true); // signal that we want to quit
		} else {
			System.out.println("Quit what?");
		}
	}
}
